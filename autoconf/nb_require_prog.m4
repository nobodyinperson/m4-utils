# NB_REQUIRE_PROG([variable],[prog],[description])
#
# - search for [prog] with AC_PATH_PROG
# - save found path to [variable]
# - fail if [prog] was not found
# - make [variable] precious with AC_ARG_VAR
# 
AC_DEFUN([NB_REQUIRE_PROG],[
AC_PATH_PROG([$2],[$1])
[test -n "$]$2["] || AC_MSG_ERROR([no $1 ($3) found])
AC_ARG_VAR([$2],[path to $1])
])

