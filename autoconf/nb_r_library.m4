# SYNOPSIS
#
#   NB_R_LIBRARY(libname[, fatal, rscript_bin])
#
# DESCRIPTION
#
#   Checks for R library.
#
#   If fatal is non-empty then absence of a LIBRARY will trigger an error.
#
# LICENSE
#
#   Copyright (c) 2008 Andrew Collier
#
#   Copying and distribution of this file, with or without modification, are
#   permitted in any medium without royalty provided the copyright notice
#   and this notice are preserved. This file is offered as-is, without any
#   warranty.

#serial 9

AC_DEFUN([NB_R_LIBRARY],[
    if test -n "$3";then
        r_lib_rscript_bin="$3"
    else
        r_lib_rscript_bin="Rscript"
    fi
    AC_MSG_CHECKING(R library $1)
    $r_lib_rscript_bin -e "library($1)" 2>/dev/null
    if test $? -eq 0;
    then
        AC_MSG_RESULT(yes)
        eval AS_TR_CPP(HAVE_RLIB_$1)=yes
    else
        AC_MSG_RESULT(no)
        eval AS_TR_CPP(HAVE_RLIB_$1)=no
        #
        if test -n "$2"
        then
            AC_MSG_ERROR(failed to find required R library $1)
            exit 1
        fi
    fi
])

